package com.example.tanmoydey.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView myListView;//variable dicleration
    String []listItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myListView =findViewById(R.id.listView);//initialization

        listItem = getResources().getStringArray(R.array.array_technology);
        final ArrayAdapter<String> adapter =new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listItem);

        myListView.setAdapter(adapter);

      //item click..
       myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String value=adapter.getItem(position);
               Toast.makeText(getApplicationContext(),value,Toast.LENGTH_SHORT).show();
           }
       });
    }
}
